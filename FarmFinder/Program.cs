﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Core;
using TBSelenium.Functions;
using TBSelenium.Services;
using TBSelenium.Web.Functions;

namespace FarmFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver("C:\\Chrome");
            driver.Url = $@"https://{FunctionConstants.PageUrl}.travian.com/dorf1.php";

            //var mainCycle = new MainCycle(driver);

            var login = new LoginFunctions(driver);
            login.LoginToPage();

            SleepManager.WaitRandom(2);

            var farmFinder = new FarmFinderFunctions(driver);

            farmFinder.FindFarms();
        }
    }
}
