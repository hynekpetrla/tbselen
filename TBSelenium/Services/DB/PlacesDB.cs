﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Models;

namespace TBSelenium.Services
{
    public class PlacesDB
    {
        // actual data to be preserved for each user
        public string User;
        public List<Place> Places;


        // metadata        
        public DateTime LastSaved;
        public int eon;

        private string dbpath;

        public static PlacesDB Load(string file = null)
        {
            string path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, file ?? "data.xml");

            PlacesDB udb;
            try
            {
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(PlacesDB));
                using (System.IO.StreamReader reader = System.IO.File.OpenText(path))
                {
                    udb = (PlacesDB)s.Deserialize(reader);
                }
            }
            catch
            {
                udb = new PlacesDB();
            }
            udb.dbpath = path;

            return udb;
        }


        public void Save()
        {
            LastSaved = System.DateTime.Now;
            eon++;
            var s = new System.Xml.Serialization.XmlSerializer(typeof(PlacesDB));
            var ns = new System.Xml.Serialization.XmlSerializerNamespaces();
            ns.Add("", "");
            System.IO.StreamWriter writer = System.IO.File.CreateText(dbpath);
            s.Serialize(writer, this, ns);
            writer.Close();
        }
    }
}
