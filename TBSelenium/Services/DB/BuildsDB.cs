﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Models;

namespace TBSelenium.Services
{
    public class BuildsDB
    {
        // actual data to be preserved for each user
        public string User;
        public List<Build> Builds;


        // metadata        
        public DateTime LastSaved;
        public int eon;

        private string dbpath;

        public static BuildsDB Load(string file = null)
        {
            string path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, file ?? "/builds.xml");

            BuildsDB udb;
            try
            {
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(BuildsDB));
                using (System.IO.StreamReader reader = System.IO.File.OpenText(path))
                {
                    udb = (BuildsDB)s.Deserialize(reader);
                }
            }
            catch
            {
                udb = new BuildsDB();
            }
            udb.dbpath = path;

            return udb;
        }


        public void Save()
        {
            LastSaved = System.DateTime.Now;
            eon++;
            var s = new System.Xml.Serialization.XmlSerializer(typeof(BuildsDB));
            var ns = new System.Xml.Serialization.XmlSerializerNamespaces();
            ns.Add("", "");
            System.IO.StreamWriter writer = System.IO.File.CreateText(dbpath);
            s.Serialize(writer, this, ns);
            writer.Close();
        }
    }
}
