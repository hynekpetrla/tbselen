﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Data;
using TBSelenium.Models;

namespace TBSelenium.Services.Farm
{
    public class SoldierService
    {
        public static Soldier GetSoldier(string name)
        {
            return ApplicationSettings.GetSoldiers().Where(x=>x.Name == name).First();
        }
    }
}
