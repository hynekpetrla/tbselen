﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TBSelenium.Core
{
    public static class SleepManager
    {
        public static void WaitRandom(int seconds)
        {
            int from = seconds * 1000;
            Thread.Sleep(new Random().Next(from, from + from / 2 + 100));
        }

        public static void WaitExact(int seconds, int? miliseconds = null)
        {
            Thread.Sleep(miliseconds ?? (seconds * 1000));
        }

        public static void WaitRandomMinutes(int minutes)
        {
            WaitRandom(minutes * 60);
        }
    }
}
