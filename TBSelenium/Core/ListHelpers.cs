﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Data;
using TBSelenium.Functions;
using TBSelenium.Models;

namespace TBSelenium.Core
{
    public class ListHelpers
    {
        public static List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }

        internal static List<Place> TakeFair(List<Place> places)
        {
            List<Place> newList = new List<Place>();

            foreach (var item in places)
            {
                var rightJoin = ApplicationTempMemory.FarmedPlaces.Where(x => x.AxisX == item.AxisX && x.AxisY == item.AxisY).OrderByDescending(x=>x.LastFarmedTime).FirstOrDefault();

                if (rightJoin != null)
                {
                    item.LastFarmedTime = rightJoin.LastFarmedTime;
                }

                newList.Add(item);
            }

            newList =  newList.OrderBy(x => x.LastFarmedTime ?? DateTime.MinValue).ToList();
            return newList;
        }
    }
}
