﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TBSelenium.Core
{
    public class StringConverter
    {
        public static string GetNumbers(string numberOfFirstElement)
        {
            numberOfFirstElement = Regex.Replace(numberOfFirstElement, @"[^\d]", "");
            return numberOfFirstElement;
        }


        public static int ToInt(string value)
        {
            value = GetNumbers(value);

            if (string.IsNullOrWhiteSpace(value))
            {
                return 0;
            }

            int i = 0;
            if (int.TryParse(value, out i))
                return i;

            return 0;
        }
    }
}
