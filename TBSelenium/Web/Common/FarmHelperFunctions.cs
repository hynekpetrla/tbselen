﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Web.Common
{
    public class FarmHelperFunctions
    {
        private IWebDriver _driver;
        private bool IsRandomSelect = true;

        public FarmHelperFunctions(IWebDriver driver)
        {
            _driver = driver;
        }

        public bool HasAttackable()
        {
            bool response = true;
            try
            {
                if (_driver.FindElement(By.ClassName("titleInHeader")).Text.Contains("Divočina"))
                    response = false;
            }
            catch { }

            try
            {
                if (_driver.FindElement(By.ClassName("titleInHeader")).Text.Contains("opuštěné údolí"))
                    response = false;
            }
            catch { }

            return response;
        }

    }
}
