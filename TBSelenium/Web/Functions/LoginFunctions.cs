﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using TBSelenium.Services;

namespace TBSelenium.Functions
{
    public class LoginFunctions
    {
        private IWebDriver _driver;

        public LoginFunctions(IWebDriver driver)
        {
            _driver = driver;
        }

        public void LoginToPage()
        {
            var name = _driver.FindElement(By.Name("name"));
            name.SendKeys(FunctionConstants.Login);
            
            var pass = _driver.FindElement(By.Name("password"));
            pass.SendKeys(FunctionConstants.Pass);

            _driver.FindElement(By.Id("s1")).Click();

        }
    }
}
