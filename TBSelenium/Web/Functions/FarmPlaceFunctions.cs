﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using TBSelenium.Core;
using TBSelenium.Data;
using TBSelenium.Models;
using TBSelenium.Services;

namespace TBSelenium.Web.Functions
{
    public class FarmPlaceFunctions
    {
        private IWebDriver _driver;
        private bool IsRandomSelect = true;

        public FarmPlaceFunctions(IWebDriver driver)
        {
            _driver = driver;
        }

        public void FarmCycle(int cycle, Vilage vilage)
        {
            var data = PlacesDB.Load($"Farm_All_{vilage.ID}.xml"); //Farm_Natar

            if (data.Places == null) return;

            if (IsRandomSelect)
                data.Places = ListHelpers.ShuffleList(data.Places);

            data.Places = ListHelpers.TakeFair(data.Places);

            foreach (var place in data.Places)
            {
                try
                {
                    //SleepManager.WaitRandom(1);

                    _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/position_details.php?x={place.AxisX}&y={place.AxisY}");

                    if (HasLostLastAttack())
                        continue;

                    if (!HasAttackable())
                        continue;

                    if (place.Type == PlaceTypes.Oaza && HasEnemy())
                        continue;
                    try
                    {
                        _driver.FindElement(By.LinkText(GetSentAttackText(place.Type))).Click();
                    }
                    catch { continue; }

                    //SleepManager.WaitRandom(1);

                    _driver.FindElements(By.Name("c"))[2].Click(); // select Loupež

                    SoldierAttackEnum sendAttact = SetSoldiers(place);

                    if (sendAttact == SoldierAttackEnum.SendAttack)
                    {
                        ConfirmAndSendAttack();
                        place.LastFarmedTime = DateTime.Now;
                        ApplicationTempMemory.FarmedPlaces.Add(place);
                    }
                    else if (sendAttact == SoldierAttackEnum.Skip)
                        continue;
                    else
                        return; // Pokud není žádný voják končí se cyklus

                }
                catch (Exception e)
                {
                    return;
                }
                SleepManager.WaitRandom(1);
            }
        }

        internal void GoToMessages()
        {
            try
            {
                SleepManager.WaitRandom(1);

                IList<string> list = new List<string>();

                GetElementsByPage(list, "1");
            }
            catch (Exception e)
            {
                return;
            }
        }

        internal void FarmFromMessages(int i, Vilage vilage)
        {
            try
            {
                IList<string> list = new List<string>();

                GetElementsByPage(list, "1");
                //GetElementsByPage(list, "2");

                foreach (var text in list)
                {
                    if (text.Contains("reportInfo carry full") && text.Contains($"{vilage.Name} vyloupil"))
                    {
                        var url = text.Substring(text.IndexOf("berichte.php?"));
                        url = url.Substring(0, url.IndexOf("toggleState=0") - 1);

                        _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/{url}");

                        _driver.FindElement(By.XPath("//*[@id=\"reportWrapper\"]/div[2]/div[3]/div[2]/div/a[2]")).Click();

                        //Duplicate
                        if (HasLostLastAttack())
                            continue;

                        if (!HasAttackable())
                            continue;

                        string type = GetTargetType();

                        if (type == PlaceTypes.Oaza && HasEnemy())
                            continue;

                        _driver.FindElement(By.LinkText(GetSentAttackText(type))).Click();

                        SleepManager.WaitRandom(1);

                        _driver.FindElements(By.Name("c"))[2].Click(); // select Loupež

                        SoldierAttackEnum sendAttact = SetSoldiers(new Place() { FarmFrequence = 6 });

                        if (sendAttact == SoldierAttackEnum.SendAttack)
                        {
                            ConfirmAndSendAttack();
                        }
                        else if (sendAttact == SoldierAttackEnum.Skip)
                            continue;
                        else
                            return; // Pokud není žádný voják končí se cyklus
                    }
                }
            }
            catch (Exception e)
            {
                return;
            }
        }

        private string GetTargetType()
        {
            try
            {
                if (_driver.FindElement(By.ClassName("titleInHeader")).Text.Contains("volná oáza"))
                    return PlaceTypes.Oaza;
            }
            catch { }

            return PlaceTypes.Player;
        }

        private void GetElementsByPage(IList<string> list, string page)
        {
            _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/berichte.php?t=0&page={page}");

            var elements = _driver.FindElements(By.ClassName("newMessage"));

            foreach (var item in elements)
            {
                var text = item.GetAttribute("innerHTML");
                if (text.Contains("reportInfo carry full"))
                    list.Add(text);
            }
        }

        private bool HasAttackable()
        {
            bool response = true;
            try
            {
                if (_driver.FindElement(By.ClassName("titleInHeader")).Text.Contains("Divočina"))
                    response = false;
            }
            catch { }

            try
            {
                if (_driver.FindElement(By.ClassName("titleInHeader")).Text.Contains("opuštěné údolí"))
                    response = false;
            }
            catch { }

            return response;
        }

        private SoldierAttackEnum SetSoldiers(Place place)
        {
            foreach (var solder in ApplicationSettings.GetSoldiers())
            {
                int numberOfSoldiers = 0;

                try { numberOfSoldiers = StringConverter.ToInt(_driver.FindElement(By.CssSelector($"input[name =\'troops[0][{solder.FieldName}]\'] + a")).Text); } catch { }

                var soldiersToAttack = solder.MinimumNumberToAttack * place.GetFarmFrequence();

                if (numberOfSoldiers < solder.MinimumNumberToAttack)
                {
                    continue;
                }
                else if (numberOfSoldiers < soldiersToAttack)
                {
                    continue;
                    //return SoldierAttackEnum.Skip;
                }

                var palkar = _driver.FindElement(By.Name($"troops[0][{solder.FieldName}]"));

                if (numberOfSoldiers >= soldiersToAttack)
                {
                    palkar.SendKeys(soldiersToAttack.ToString());
                }

                return SoldierAttackEnum.SendAttack;
            }

            return SoldierAttackEnum.Finish;
        }

        private void ConfirmAndSendAttack()
        {
            //Send attack
            _driver.FindElement(By.Name("s1")).Click();

            SleepManager.WaitExact(0, 500);

            //Confirm send attack
            _driver.FindElement(By.Id("btn_ok")).Click();
        }


        private string GetSentAttackText(string type)
        {
            if (type == PlaceTypes.Oaza)
                return "Prozkoumat volnou oázu";

            return "Poslat jednotky";
        }

        private bool HasEnemy()
        {
            try
            {
                var element = _driver.FindElement(By.Id("troop_info"));

                var troop = element.Text;

                if (element.Text.Contains("žádné"))
                    return false;

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool HasLostLastAttack()
        {
            try
            {
                var iReports = _driver.FindElements(By.ClassName("iReport"));

                var classElement = iReports[0].GetAttribute("class");
                return classElement.Contains("iReport2") || classElement.Contains("iReport3");
            }
            catch
            {
                return false;
            }
        }

        public void FillData()
        {
            var data = PlacesDB.Load();

            data.Places = new List<Models.Place>()
            {
                new Models.Place()
                {
                    AxisX = "-97",
                    AxisY = "-29"
                }
            };

            data.Save();
        }
    }
}
