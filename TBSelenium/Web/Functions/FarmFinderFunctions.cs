﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using TBSelenium.Core;
using TBSelenium.Data;
using TBSelenium.Models;
using TBSelenium.Services;
using TBSelenium.Web.Common;

namespace TBSelenium.Web.Functions
{
    public class FarmFinderFunctions
    {
        private IWebDriver _driver;
        private readonly FarmHelperFunctions _farmHelper;
        private bool IsRandomSelect = true;

        public FarmFinderFunctions(IWebDriver driver)
        {
            _driver = driver;
            _farmHelper = new FarmHelperFunctions(_driver);
        }

        public void FindFarms()
        {
            int middlePositionX = -59;
            int middlePositionY = -50;
            //int middlePositionX = -46;
            //int middlePositionY = -33;

            int range = 20;

            List<int> rangeX = Enumerable.Range(middlePositionX - range, range * 2).ToList();
            List<int> rangeY = Enumerable.Range(middlePositionY - range, range * 2).ToList();

            SleepManager.WaitRandom(2);

            var usedFarm = PlacesDB.Load("used_farm.xml");
            var db = PlacesDB.Load("new_farm.xml");

            foreach (var x in rangeX)
            {
                foreach (var y in rangeY)
                {
                    try
                    {
                       // SleepManager.WaitRandom(0);

                        _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/position_details.php?x={x}&y={y}");

                        if (!_farmHelper.HasAttackable())
                            continue;

                        var type = GetFarmType();
                        var population = GetPopulation();

                        if (type == PlaceTypes.Empty)
                        {
                            continue;
                        }
                        else if (type == PlaceTypes.Player && IsPlayerStrong(population))
                        {
                            continue;
                        }
                        else if (type == PlaceTypes.Oaza /*&& HasEnemy()*/)
                        {
                            continue;
                        }

                        string alliance = GetAliance();

                        if (!usedFarm.Places.Any(pl => pl.AxisX == x.ToString() && pl.AxisY == y.ToString())
                            && !IsFriendAlliance(alliance))
                        {
                            db.Places.Add(new Place()
                            {
                                AxisX = x.ToString(),
                                AxisY = y.ToString(),
                                Type = type,
                                FarmFrequence = 1,
                                Info = population?.ToString() + " " + HasProtection() + " Aliance: " + alliance
                            });
                            db.Save();
                        }
                    }
                    catch (Exception e)
                    {
                        var exception = 1;
                    }
                }
            }

            db.Save();//-39 -26
        }

        private bool IsFriendAlliance(string alliance)
        {
            if (string.IsNullOrWhiteSpace(alliance))
                return false;

            List<string> friends = new List<string>()
            {
                "LE","LE II","LE III","LE IV",
                "TT","TT II",
                "#SM",
                "LS-L",
                "IT.",
                "FAME",
                "OBLUDY",
                "SMASH",
                "--------",
                "HRA",
                "K.I."
            };

            if (friends.Contains(alliance))
                return true;
            else
                return false;
        }

        private string GetAliance()
        {
            try
            {
                var element = _driver.FindElement(By.CssSelector("#village_info > tbody > tr:nth-child(2) > td > a")).Text;
                if (element != null)
                    return element;
                else
                    return string.Empty;

            }
            catch
            {
                return string.Empty;
            }
        }

        private string HasProtection()
        {
            try
            {
                var element = _driver.FindElement(By.CssSelector("#tileDetails > div.detailImage.vid3 > div:nth-child(1) > div:nth-child(2) > span.disabled")).Text.Contains("Poslat jednotky");
                if (element)
                    return " Má ochranu ";
                else
                    return string.Empty;

            }
            catch
            {
                return string.Empty;
            }
        }

        private bool IsPlayerStrong(int? population)
        {
            try
            {
                if (population <= ApplicationSettings.MaxFarmPopulation)
                    return false;

                return true;
            }
            catch
            {
                return true;
            }
        }

        private int? GetPopulation()
        {
            try
            {
                var element = _driver.FindElement(By.XPath("//*[@id=\"village_info\"]/tbody/tr[4]/td"));

                var troop = element.Text;
                return Int32.Parse(troop);
            }
            catch
            {
                return null;
            }
        }

        private string GetFarmType()
        {
            try
            {
                var element = _driver.FindElement(By.ClassName("oasis"));
                if (element != null)
                    return PlaceTypes.Oaza;
            }
            catch { }

            try
            {
                var element = _driver.FindElement(By.ClassName("village"));
                if (element != null)
                {
                    //if (_driver.FindElement(By.XPath("//*[@id=\"village_info\"]/tbody/tr[1]/td")).Text.Contains("Nataři"))
                    //   return PlaceTypes.Oaza;
                    return PlaceTypes.Player;
                }
            }
            catch { }

            return PlaceTypes.Empty;
        }

        private void GetElementsByPage(IList<string> list, string page)
        {
            _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/berichte.php?t=0&page={page}");

            var elements = _driver.FindElements(By.ClassName("newMessage"));

            foreach (var item in elements)
            {
                list.Add(item.GetAttribute("innerHTML"));
            }
        }

        private void ConfitAndSendAttack()
        {
            //Send attack
            _driver.FindElement(By.Name("s1")).Click();

            //Confirm send attack
            _driver.FindElement(By.Id("btn_ok")).Click();
        }


        private string GetSentAttackText(string type)
        {
            if (type == PlaceTypes.Oaza)
                return "Vyloupit volnou oázu";

            return "Poslat jednotky";
        }

        private bool HasEnemy()
        {
            try
            {
                var element = _driver.FindElement(By.Id("troop_info"));

                var troop = element.Text;

                if (element.Text.Contains("žádné"))
                    return false;

                return true;
            }
            catch
            {
                return true;
            }
        }

        private bool HasLostLastAttack()
        {
            try
            {
                var iReports = _driver.FindElements(By.ClassName("iReport"));

                var classElement = iReports[0].GetAttribute("class");
                return classElement.Contains("iReport2") || classElement.Contains("iReport3");
            }
            catch
            {
                return false;
            }
        }

        public void FillData()
        {
            var data = PlacesDB.Load();

            data.Places = new List<Models.Place>()
            {
                new Models.Place()
                {
                    AxisX = "-97",
                    AxisY = "-29"
                }
            };

            data.Save();
        }
    }
}
