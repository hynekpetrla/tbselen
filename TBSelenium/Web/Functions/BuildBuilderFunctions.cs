﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Core;
using TBSelenium.Models;
using TBSelenium.Services;

namespace TBSelenium.Web.Functions
{
    public class BuildBuilderFunctions
    {
        private IWebDriver _driver;

        public bool IsRandomSelect = true;

        public BuildBuilderFunctions(IWebDriver driver)
        {
            _driver = driver;
        }

        public void BuildCycle(Vilage vilage)
        {
            var data = BuildsDB.Load($"/builds_All_{vilage.ID}.xml");


            _driver.Navigate().GoToUrl($"https://{FunctionConstants.PageUrl}.travian.com/dorf1.php");
            if (IsBuildRunning())
                return;

            if (IsRandomSelect)
                data.Builds = ListHelpers.ShuffleList(data.Builds);

            foreach (var build in data.Builds)
            {
                try
                {
                    SleepManager.WaitRandom(1);

                    _driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/build.php?id={build.BuildID}");

                    if (IsBuildRunningPlace())
                        return;

                    if (!HasEnoughtResources())
                        continue;

                    var confirmButton = _driver.FindElement(By.CssSelector("button[type=button][value^=\"Rozšíření na úroveň\"]"));

                    if (confirmButton != null)
                        confirmButton.Click();

                }
                catch (Exception e)
                {

                }
                SleepManager.WaitRandom(1);
            }
        }

        private bool IsBuildRunningPlace()
        {
            return ElementSelectors.IsElementPresent(_driver, By.XPath("//*/h5[contains(text(), 'Postavit se stavbyvedoucím')]"));
        }

        private bool IsBuildRunning()
        {
            //return false;
            return ElementSelectors.IsElementPresent(_driver, By.XPath("//*/h5[contains(text(), 'Ve výstavbě')]"));
        }

        private bool HasEnoughtResources()
        {
            try
            {
                return _driver.FindElement(By.CssSelector("button[type=button][value^=\"Postavit se stavbyvedoucím\"]")) == null;
            }
            catch
            {
                return true;
            }
        }
    }
}
