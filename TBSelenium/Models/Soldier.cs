﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Models
{
    public class Soldier
    {
        public string Name { get; set; }

        public int MinimumNumberToAttack { get; set; }
        public string FieldName { get; internal set; }
    }
}
