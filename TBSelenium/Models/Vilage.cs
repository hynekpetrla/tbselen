﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Models
{
    public class Vilage
    {
        public Vilage() { }

        public Vilage(string name, int id, string urlParam, bool isAcademy = false, bool isGoldMine = false)
        {
            Name = name;
            ID = id;
            UrlParam = urlParam;
            IsAcademy = isAcademy;
            IsGoldMine = isGoldMine;
        }

        public string Name { get; set; }
        public int ID { get; set; }
        public string UrlParam { get; set; }
        public bool IsAcademy { get; set; }
        public bool IsGoldMine { get; set; }
    }
}
