﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Models
{
    public class Place
    {
        public string AxisX { get; set; }

        public string AxisY { get; set; }

        public string Type { get; set; }

        public int? FarmFrequence { get; set; }

        public DateTime? LastFarmedTime { get; set; }

        public string Info { get; set; }

        public int GetFarmFrequence()
        {
            return FarmFrequence ?? 1;
        }
    }
}
