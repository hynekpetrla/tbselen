﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Models
{
    public class SoldierTypes
    {
        public static string Palkar = "Palkar";

        public static string Ostepar = "Ostepar";

        public static string Jezdec = "Jezdec";

        public static string Teuton = "Teuton";

        public static string Rytir = "Rtíř";
    }
}
