﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Safari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBSelenium.Core;
using TBSelenium.Data;
using TBSelenium.Functions;
using TBSelenium.Models;
using TBSelenium.Services;
using TBSelenium.Web.Functions;

namespace TBSelenium
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver("C:\\Chrome");
            driver.Url = $@"https://{FunctionConstants.PageUrl}.travian.com/dorf1.php";

            //var mainCycle = new MainCycle(driver);

            var login = new LoginFunctions(driver);
            login.LoginToPage();

            SleepManager.WaitRandom(2);

            var farm = new FarmPlaceFunctions(driver);
            var builds = new BuildBuilderFunctions(driver);

            int i = 0;
            while (i <= 2000)
            {
                foreach (var vilage in ApplicationSettings.GetVillages())
                {
                    try
                    {
                        if (vilage.IsGoldMine)
                            SleepManager.WaitRandom(30);

                        driver.Navigate().GoToUrl($@"https://{FunctionConstants.PageUrl}.travian.com/dorf1.php?newdid={vilage.UrlParam}&");

                        //farm.FarmFromMessages(i, vilage);

                        //farm.FarmCycle(i, vilage);

                        SleepManager.WaitRandom(1);


                        if (!vilage.IsGoldMine && 
                            (i % 2 == 0 || vilage.IsAcademy))
                        {
                            builds.BuildCycle(vilage);
                        }

                        if (i % 2 == 0)
                        {
                            // Reset uložených farem a znovu promíchání
                            ApplicationTempMemory.FarmedPlaces = new List<Place>();
                        }
                    }
                    catch (Exception e) { }
                }

                farm.GoToMessages();
                SleepManager.WaitRandomMinutes(30);
                i++;
            }

            //TODO: Random výběr farm
        }
    }
}
