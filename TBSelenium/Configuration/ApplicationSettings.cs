﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Models;

namespace TBSelenium.Data
{
    public class ApplicationSettings
    {
        public static IList<Soldier> GetSoldiers()
        {
            var soldiers = new List<Soldier>();

            soldiers.Add(new Soldier()
            {
                Name = SoldierTypes.Palkar,
                FieldName = "t1",
                MinimumNumberToAttack = 4
            });
            /*
            soldiers.Add(new Soldier()
            {
                Name = SoldierTypes.Ostepar,
                FieldName = "t2",
                MinimumNumberToAttack = 10
            });

            soldiers.Add(new Soldier()
            {
                Name = SoldierTypes.Teuton,
                FieldName = "t6",
                MinimumNumberToAttack = 4
            });

            soldiers.Add(new Soldier()
            {
                Name = SoldierTypes.Rytir,
                FieldName = "t5",
                MinimumNumberToAttack = 4
            });
            */
            return soldiers;
        }

        public static IEnumerable<Vilage> GetVillages()
        {
            yield return new Vilage("Perkos1", 100, "2887", false, true);
            yield return new Vilage("Perkos1", 200, "2887", false, true);
            yield return new Vilage("Perkos1", 100, "2887", false, true);
            yield return new Vilage("Perkos1", 1, "2887");
            yield return new Vilage("Perkos1", 100, "2887", false, true);

            yield return new Vilage("Perkos2", 100, "19006", false, true);
            yield return new Vilage("Perkos2", 2, "19006");
            yield return new Vilage("Perkos2", 100, "19006", false, true);

            yield return new Vilage("Perkos3", 3, "22307");
            yield return new Vilage("Perkos4", 4, "23676");
            yield return new Vilage("Perkos5", 5, "24658", true);
            yield return new Vilage("Perkos6", 6, "25108", true);
            yield return new Vilage("Perkos7", 7, "25331", true);
        }

        private static void AddVillage(List<Vilage> village, string name, int id, string urlParam, bool isAcademy, bool isGoldMine)
        {
            village.Add(new Vilage()
            {
                Name = name,
                ID = id,
                UrlParam = urlParam,
                IsAcademy = isAcademy,
                IsGoldMine = isGoldMine
            });
        }

        public static int MaxFarmPopulation = 450;
    }
}
