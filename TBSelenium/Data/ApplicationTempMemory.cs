﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSelenium.Models;

namespace TBSelenium.Data
{
    public static class ApplicationTempMemory
    {
        public static List<Place> FarmedPlaces { get; set; } = new List<Place>();
    }
}
