﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSelenium.Services
{
    public class FunctionConstants
    {
        public static string PageUrl { get { return ConfigurationManager.AppSettings["Server"]; } }

        public static string Login { get { return ConfigurationManager.AppSettings["DefaultUserName"]; } }

        public static string Pass { get { return ConfigurationManager.AppSettings["DefaultPwd"]; } } //ConfigurationManager.AppSettings["MySetting"]

    }
}
